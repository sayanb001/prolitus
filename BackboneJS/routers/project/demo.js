
// -- data model and collections --
var Boat = Backbone.Model.extend()
var BoatsList = Backbone.Collection.extend({
    model: Boat
})

var Car = Backbone.Model.extend()
var CarsList = Backbone.Collection.extend({
    model: Car
})

var boats = new BoatsList([
    new Boat({'reg': 'W-ABC123', 'color': 'White'}),
    new Boat({'reg': 'W-PPS132', 'color': 'Cyan'}),
    new Boat({'reg': 'W-KLJ451', 'color': 'Black'})
])

var cars = new CarsList([
    new Car({'reg': 'ABC123', 'color': 'Red'}),
    new Car({'reg': 'PPS132', 'color': 'Blue'}),
    new Car({'reg': 'KLJ451', 'color': 'Silver'})
])

// -- views --
var HomePageView = Backbone.View.extend({
    render: function(){
        this.$el.html("Welcome to the Cars and Boats Page")
        return this
    }
})

var CarPageView = Backbone.View.extend({
    render: function(){
        this.$el.html("Welcome to the Car Page")
        var mView = new GenericListView({ model: cars})
        this.$el.append(mView.render().$el)
        return this
    }
})

var BoatPageView = Backbone.View.extend({
    render: function(){
        this.$el.html("Welcome to the Boat Page")
        var mView = new GenericListView({ model: boats})
        this.$el.append(mView.render().$el)
        return this
    }
})

// -- views util --
var GenericPageView = Backbone.View.extend({
    render: function(){
        this.$el.html("Welcome to the Car Page")
        var mView = new GenericListView({ model: cars})
        this.$el.append(mView.render().$el)
        return this
    }
})
var GenericView = Backbone.View.extend({
    tagName: 'li',
    render: function(){
        this.$el.html(this.model.get('reg') + " - " +
        this.model.get('color'))
        return this
    }
})
var GenericListView = Backbone.View.extend({
    tagName: 'ul',
    id: 'content-list',
    render: function(){
        let self = this
        this.model.each(function(ListItem){
            var ListItemView = new GenericView({model: ListItem})
            self.$el.append(ListItemView.render().$el)
        })
        return this
    }
})

var NavView = Backbone.View.extend({
    events: {
        'click': 'itemClick'
    },
    itemClick: function(e){
        var item = $(e.target)
        router.navigate(item.attr('data-url'), {trigger: true})
    }
})

// -- router --

var AppRouter = Backbone.Router.extend({
    routes:{
        "Home": "showHome",
        "Boats": "showBoats",
        "Cars": "showCars"
    },

    showHome: function(){
        var view = new HomePageView({el: '#content'})
        view.render()
    },

    showBoats: function(){
        var view = new BoatPageView({el: '#content'})
        view.render()
    },

    showCars: function(){
        var view = new CarPageView({el: '#content'})
        view.render()
    },
})

var router = new AppRouter()
Backbone.history.start()

var nav = new NavView({el: '#nav'})
nav.render()



