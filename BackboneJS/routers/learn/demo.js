var Home = Backbone.View.extend({
    render: function(){
        this.$el.html("Home Page")
        return this
    }
})

var About = Backbone.View.extend({
    render: function(){
        this.$el.html("About Page")
        return this
    }
})

var Content = Backbone.View.extend({
    render: function(){
        this.$el.html("Contact Page")
        return this
    }
})

var AppRouter = Backbone.Router.extend({
    routes: {
        'Home': 'showHome',
        'About': 'showAbout',
        'Contact': 'showContact'
    },

    showHome: function(){
        var view = new Home({el: '#content'})
        view.render()
    },
    showAbout: function(){
        var view = new About({el: '#content'})
        view.render()
    },
    showContact: function(){
        var view = new Content({el: '#content'})
        view.render()
    }
})

var router = new AppRouter()
Backbone.history.start()

var NavView = Backbone.View.extend({
    events: {
        "click": "itemClick"
    },
    itemClick: function(e){
        var myItem = $(e.target)
        router.navigate(myItem.attr('data-url'), {trigger: true})
        var content = $("#content")
        content.attr('bgcolor','red')
    }
})

var navView = new NavView({el:"#menu"})
navView.render()
