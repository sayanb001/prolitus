var Vehicle = Backbone.Model.extend({
    // initialize: function(){
        // if (!this.isValid()){
            // console.log(this.cid + ": Error: " + this.validationError)
        // } else {
            // console.log(this.cid + ": Created")
        // }
    // },
    urlRoot: '/api/vehicles',

    start: function(){
        console.log("Vehicle Started " + this.get('registrationNumber'))

    },

    validate: function(attrs){
        if(!attrs.registrationNumber){
            return "Unique registration number is required"
        }
    }
})

function checkValid(car){
    if (!car.isValid()){
        console.log(car.cid + ": Error: " + car.validationError)
    } else {
        console.log(car.cid + ": Created")
    }
}
// ---------------------------------------------
var Car = Vehicle.extend();

var car1 = new Car();

var car2 = new Car({'registrationNumber':'ABC123'})
car2.start()

var car3 = new Car({
    'registrationNumber':'XYZ908',
    'color':'Blue'
})
car3.unset('registrationNumber')

checkValid(car3)

car3.set('registrationNumber', 'XLI887')
checkValid(car3)
car3.start()


