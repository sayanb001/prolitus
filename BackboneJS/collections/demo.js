var Vehicle = Backbone.Model.extend({
    urlRoot: '/api/vehicle/',
    start: function(){
        console.log("Vehicle Started " + this.get('registrationNumber'))
    },
    validate: function(attrs){
        if(!attrs.registrationNumber){
            return "Unique registration number is required"
        }
    }
})

function checkValid(car){
    if (!car.isValid()){
        console.log(car.cid + ": Error: " + car.validationError)
    } else {
        console.log(car.cid + ": Created")
    }
}
// ---------------------------------------------

var Vehicles = Backbone.Collection.extend({
    model: Vehicle,
    url: "/api/vehicles/"
})
var Vehicles_Col = new Vehicles()

var Car = Vehicle.extend();
var car1 = new Car({'registrationNumber': 'XLI887', 'color': 'Blue'})
var car2 = new Car({'registrationNumber': 'ZNP123', 'color':'Grey'})
var car3 = new Car({'registrationNumber': 'XUV456', 'color': 'Blue'})

Vehicles_Col.add(car1)
Vehicles_Col.add(car2)
Vehicles_Col.add(car3)

var blueCars = Vehicles_Col.where({'color':'Blue'})
console.log(blueCars)

var thatCar = Vehicles_Col.findWhere({'registrationNumber':'XLI887'})
console.log(thatCar)

Vehicles_Col.remove(thatCar)
console.log(Vehicles_Col.toJSON())

console.log("Iterate")
Vehicles_Col.each(function(car){
    console.log(car)
})
