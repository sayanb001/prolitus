var Vehicle = Backbone.Model.extend({
    validate: function(attrs){
        if(!attrs.registrationNumber){
            console.log("Error No Registration Number")
        }
    }
})

var Vehicles = Backbone.Collection.extend({
    model: Vehicle,
})

var VehicleView = Backbone.View.extend({
    initialize: function(){
        this.model.on('change', this.render, this)
    },

    attributes:{
        "data-color":"",
    },

    tagName: 'li',
    events:{
        'click': "onBook",
        'click .del' : "onDel"
    },

    onBook: function(){
        console.log(this.model.get('registrationNumber') + " Booked")
    },

    onMark: function(e){
        e.stopPropagation()
        console.log(this.model.get('registrationNumber') + " Marked")
    },

    onDel : function(e){
        e.stopPropagation()
        this.$el.remove()

    },

    render: function(){
        this.$el.attr('data-color', this.model.get('color'))
        this.$el.attr('class','vehicle')
        this.$el.attr('id', this.model.cid)
        this.$el.html(this.model.get('registrationNumber') + " - " +
            this.model.get('color') +
            "&nbsp;<button class='book'>Book this car</button>" +
            "&nbsp;<button class='del'>Delete</button>"
        )
        return this
    }
})

var VehiclesView = Backbone.View.extend({
    initialize: function(){
        this.model.on("add", this.onCarAdded, this)
        this.model.on("remove", this.onCarRemoved, this)
    },

    onCarRemoved: function(car){
        this.$el.find("li#"+car.cid).remove()
    },

    onCarAdded: function(car){
        var newCar = new VehicleView({model: car})
        this.$el.append(newCar.render().$el)
    },

    render: function(){
        var self = this
        this.model.each(function(v){
            var vehicleEach = new VehicleView({model: v})
            self.$el.append(vehicleEach.render().$el)
        })
        return this
    }
})

// ---------------------------------------------------------------------

var vehicles = new Vehicles([
    new Vehicle({'registrationNumber':'ABC123', 'color':'Blue'}),
    new Vehicle({'registrationNumber':'CLO890', 'color':'Red'}),
    new Vehicle({'registrationNumber':'XAC311', 'color':'Blue'})
])

var vehiclesView = new VehiclesView({ el: '#cars', model: vehicles })
vehiclesView.render()
