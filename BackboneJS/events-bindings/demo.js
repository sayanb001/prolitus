var person = {
    name :"Sayan",
    age: 22,
    walk: function(){
        this.trigger("walking", {speed: 1, startTime: "9:00"})
    }


}
_.extend(person, Backbone.Events)

person.on("walking", function(args){
    console.log("person is walking")
    console.log(args)
})

person.walk()

person.off("walking")

person.walk()

person.once("walking", function(){
    console.log("person is walking")
})
person.walk()
person.walk()
