var TodoView = Backbone.View.extend({
    initialize: function() {
        if (!(this.model.get('title'))) {
            throw new Error("TodoView: Title not specified")
        }

        this.model.on("change", this.render, this)
    },

    tagName: 'li',

    events: {
        "click": "onClickToggle",
        "click #del" : "onClickDelete"
    },

    onClickToggle: function(){
        this.model.toggle()
        this.model.save()
        // console.log(this.model.toJSON())
    },

    onClickDelete : function(e){
        e.stopPropagation()
        this.model.destroy()
    },

    render: function() {
        this.$el.toggleClass("completed", this.model.get('completed'))
        this.$el.attr('id', this.model.get('id'))
        var $toggle = this.$("#toggle")
        var checked = (this.model.get('completed')) ? "checked" : ""
        this.$el.html("<input hidden type='checkbox' id='toggle' " + checked+ ">" + this.model.escape('title') + "&nbsp;<button id='del'>Delete</button>")
        return this
    }
})

var TodoListView = Backbone.View.extend({
    initialize: function() {
        if (!(this.model)) {
            throw new Error("TodoListView: Model not specified")
        }

        this.model.on("add", this.onTodoAdd, this)
        this.model.on("remove", this.onTodoRemove, this)
    },

    onTodoAdd: function(todo){
        var myTodo = new TodoView({model : todo})
        this.$el.append(myTodo.render().$el)
    },

    onTodoRemove: function(todo){
        this.$("li#" + todo.id).remove()
    },

    events: {
        'click #add' : 'onClickAdd',
        'keypress #txt' : 'onKeyPress'
    },

    onKeyPress: function(e){
        if(e.keyCode == 13){
            this.onClickAdd()
        }
    },

    onClickAdd: function(){
        var $txt = this.$('#txt')
        if($txt.val() != ''){
            var newTodo = new Todo({title: $txt.val()})
            if (newTodo){
                this.model.add(newTodo)
                $txt.val("")
                newTodo.save()
                // newTodo.create()
            }
        }
        console.log(Todos.length)
    },

    tagName: 'ul',
    id: 'todo-list',
    render: function() {
        var self = this
        this.$el.append("<span><input type='text' placeholder='Enter Task' id='txt' autofocus></span>")
        this.$el.append("<span><button id='add'>Add</button></span>")
        this.model.each(function(todo) {
            var newtodo = new TodoView({ model: todo })
            self.$el.append(newtodo.render().$el)
        })
        return this
    }
})
