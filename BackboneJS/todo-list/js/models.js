var Todo = Backbone.Model.extend({
    urlRoot: 'https://jsonplaceholder.typicode.com/todos/',
    defaults: {
        completed: false,
    },
    toggle: function() {
        this.set('completed', !this.get('completed'))
    },
})
