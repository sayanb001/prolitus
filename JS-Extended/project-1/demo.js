function makeCard({name, addr, email}){
    let holder = $("#card-holder")
    let card = `<div id='card'>\n
        <div id="close" style="text-align:right; color:red; width: auto;display:block; display:relative; float:right">X</div>\n
        <h3>${name}</h3> <hr> <h5>${addr}</h5> <h5>${email}</h5> </div>`
    $("#card-holder").append(card)
}



$(document).ready(function() {
    $("#submit").on("click", function(){
        let name = $("#name").val();
        let addr = $("#addr").val();
        let email = $("#email").val();
        const ob = {name, addr, email}
        makeCard(ob)
    })

    $(document).on("click", "#close", function(){
        console.log($(this).parent()[0].remove())
    })

})
