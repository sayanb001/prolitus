const url = "https://api.themoviedb.org/3/search/movie/"
const api_key = "f034dfad24c84d007c66d09bb2252183"

function reGen(data){
    let result = data.results
    $("#result").empty()
    console.log(result.length)
    if(result != 0){
        for(let r of result){
            if(r.poster_path != null){
                let base_uri = "http://image.tmdb.org/t/p/w185"
                let bg_path = r.poster_path
                let title = r.original_title
                let f_img_path = base_uri +bg_path
                let c = `<div id="result-card"><div id="r-img"><img id="i" alt="${title}" src="${f_img_path}"></div><div id="r-title"><b>${title}</b></div></div>`
                // console.log(c)
                $("#result").append(c)
            }
        }

    }
    else{
        $("#result").html("<b style='color:white'>No results</b>")
    }
}

function findAPI(query) {
    const f_url = `${url}?api_key=${api_key}&query=${query}`
    $.ajax({
        url: f_url,
        method: 'GET',
        success: function(data, status, xhr){
            if(xhr.readyState == 4 && xhr.status == 200){
                console.log(data)
                reGen(data)
            }
        },
        error: function(data, status, xhr) {
            console.log("An Error occurred")
            console.log(xhr)
        }

    })
}


$(document).ready(function(){
    $("#search-txt").on("keypress",function(e){
        if(e.keyCode == 13){
           findAPI($(this).val())
            $(this).val('')
        }
    })

    $("#regen").on("click",function(){
        let c = '<div id="result-card"><div id="r-img"><img src="https://picsum.photos/100"></div><div id="r-title"><b>Title</b></div></div>'
        $("#result").append(c)
    })

})
