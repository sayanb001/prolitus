#! /usr/bin/python3

def get_estimate(h, w, c):
    total =  h  * w
    est = h * w * c
    print(f"Total estimate to cover {total} sq meter is Rs. {est} ")




if __name__ == "__main__":
    try:
        h = int(input("Enter Height of area: "))
        w = int(input("Enter Width of area: "))
        c = int(input("Enter cost of covering 1sq meter: "))

        get_estimate(h, w, c)
    except Exception as e:
        print(e)



