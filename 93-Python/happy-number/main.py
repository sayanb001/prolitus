#! /usr/bin/python3
def sq_sum(num):
    sum = 0
    while num > 0:
        sum += (num % 10) * (num % 10)
        num = num // 10
    return sum

if __name__ == '__main__':
    num = int(input("Enter Numbers: "))
    s = set()
    status = True
    while num != 1:
        s.add(num)
        num = sq_sum(num)
        if num in s:
            status = False
            break
    print(status)
