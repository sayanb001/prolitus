#! /usr/bin/python3

class Graph:
    def __init__(self, V):
        self.V = V
        self.adj = [[] for x in range(V)]

    def addEdge(self, u, v):
        self.adj[u].append(v)
        self.adj[v].append(u)

    def dfs(self, temp, v, visited):
        visited[v] = True
        temp.append(v)
        for i in self.adj[v]:
            if visited[i] == False:
                temp = self.dfs(temp, i, visited)
        return temp

    def connectedComponenets(self):
        visited = []
        cc = []
        for i in range(self.V):
            visited.append(False)
        for v in range(self.V):
            if visited[v] == False:
                temp = []
                cc.append(self.dfs(temp, v, visited))
        return cc

if __name__ == '__main__':
    g = Graph(5)
    g.addEdge(1,0)
    g.addEdge(1,2)
    # g.addEdge(2,3)
    g.addEdge(3,4)
    cc = g.connectedComponenets()
    print("Connected Components")
    print(cc)
