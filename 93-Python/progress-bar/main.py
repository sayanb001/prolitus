#! /usr/bin/python3
from tkinter import *
from tkinter.ttk import *
import time

root = Tk()
root.title("Progressbar")
root.geometry('300x200')

BG = '#2f343f'
print(BG)

progress = Progressbar(root, orient=HORIZONTAL, length=100, mode='determinate')
progress.pack(pady=10)
lbl = Label(root, text="IDLE")
lbl.pack(pady=15)

def bar():
    progress['value'] = 0;
    while(progress['value'] < 100):
        lbl['text'] = "RUNNING {0}%".format(progress['value'])
        progress['value'] += 10
        root.update_idletasks()
        time.sleep(1)
    lbl['text'] ="DONE"

Button(root, text='START', command=bar).pack(pady=10)
root.mainloop()

