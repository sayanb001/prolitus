#! /usr/bin/python3
import subprocess
from math import pi
__pi = str(pi)

subprocess.run(['clear'])

def getPi():
    while True:
        try:
            num = int(input("Enter Number b/w (1-15): "))
            if num > 15 or num < 1:
                subprocess.run(['clear'])
                print("Retry!")
                continue
            print(__pi[:num+2])
            break
        except Exception as e:
            subprocess.run(['clear'])
            print("Only Numeric value allowed")


if __name__ == "__main__":
    getPi()
