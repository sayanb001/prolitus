#! /usr/bin/python3
from math import sqrt

def prime(num):
    if not num % 2:
        while not num % 2:
            print(2)
            num /= 2

    for i in range(3, int(sqrt(num)) + 1, 2):
        while not num % i:
            print(i)
            num /= i

    if num > 1 :
        print(int(num))

if __name__ == "__main__":
    try:
        num = int(input("Enter a number: "))
        prime(num)
    except Exception as e:
        print(e)

