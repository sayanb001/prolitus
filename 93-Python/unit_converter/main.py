
class UnitConverter:
    def __init__(self):
        pass

    def temp_convert(self):
        # try to put this in while(True)
        # if t != 1 or t != 2:
            # print('Wrong choice, try again')

        t = int(input('\n1) C to F\n2) F to C\n'))
        i = int(input('Enter value: '))
        if t == 1:
            r = ((i * 9) / 5) + 32
            print(str(r) + '\u00b0 F')
        if t == 2:
            r = ((i - 32) * 5) / 9
            print(str(r) + '\u00b0 C')

    def currency_convert(self):
        c_map = {'USD':0.013, 'EUR': 0.012, 'YEN': 1.40, 'RMB': 0.094}
        print('Available conversions:')
        for i in c_map.keys():
            print(i, end=' ')

        val, currency_code = str(input('\nEnter value' +
                          '[format: <value> <currency_code>]: ')).split()

        print(f'INR -> {currency_code}: ', int(val)*c_map.get(currency_code))


    def vol_convert(self):
        v_map = {'L-ML':1000, 'ML-L':0.001, 'ML-OZ':0.0334, 'OZ-ML':29.573}
        print('Available conversions:')
        for i in v_map.keys():
            print(i, end=' ')
        val, vol_code = str(input('\nEnter value' +
                          '[format: <value> <volume_code>]: ')).split()
        vol_code_1 = vol_code.split('-')[0]
        vol_code_2 = vol_code.split('-')[1]
        print(f'{val}{vol_code_1} :', int(val)*v_map.get(vol_code), f"{vol_code_2}")

if __name__ == '__main__':
    uc = UnitConverter()
    option = int(input('1)Temperature\n2)Currency\n3)Volume\n:'))
    if option == 1:
        uc.temp_convert()
    if option == 2:
        uc.currency_convert()
    if option == 3:
        uc.vol_convert()
