#! /usr/bin/python3
from playsound import playsound
from time import sleep
class AlarmClock:
    def __init__(self, audio_path):
        self.audio_path = audio_path

    def set_alarm(self, time):
        self.m_time = time
        i_time = self.m_time.split(':')
        min_time = int(i_time[0]) * 60 + int(i_time[1])

        print(f'Setting for alarm for {self.m_time} minutes\n')
        sleep(float(min_time))
        print("Wake Up !!...")
        playsound(self.audio_path)

if __name__ == "__main__":
    ac = AlarmClock("/home/sayan/Music/biosphere - it's a great big world.mp3")
    ac.set_alarm('0:10')


