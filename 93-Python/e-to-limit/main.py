#! /usr/bin/python3
import subprocess
from math import e
__e = str(e)

subprocess.run(['clear'])

def get_e():
    while True:
        try:
            num = int(input("Enter Number b/w (1-15): "))
            if num > 15 or num < 1:
                subprocess.run(['clear'])
                print("Retry!")
                continue
            print(__e[:num+2])
            break
        except:
            subprocess.run(['clear'])
            print("Only Numeric value allowed")


if __name__ == "__main__":
    get_e()
