#! /usr/bin/python3

def getPrime():
    try:
        num = int(input("Enter a number (less than 10^7): "))
    except Exception as e:
        print(e)

    p = 2
    out = ""
    seive = [True for i in range(num + 1)]
    seive[0] = False
    seive[1] = False
    while (p * p <= num):
        if seive[p]:
            for i in range(p*p, num+1, p):
                if seive[i]:
                    seive[i] = False
        p +=1

    for i,j  in enumerate(seive):
        if seive[i]:
            out += str(i)
            out += " "
    print(out)

if __name__ == "__main__":
    getPrime()
