#! /usr/bin/python3
import random
import string

def gen_pass(length, count):
    for _ in range(count):
        yield ''.join([random.choice(string.ascii_letters + string.digits)[0] for _ in range(length)])

#  iterator
test_gen = gen_pass(length=5, count=10);
print(next(test_gen))
print(next(test_gen))

#--------
print()
#--------

for i in iter(gen_pass(8,5)):
    print(i)
