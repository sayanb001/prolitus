#! /usr/bin/python3
import unittest
from string_test import *

class MyTest(unittest.TestCase):

    def test_cap(self):
        string = "hello world"
        expected = "Hello world"
        self.assertEqual(make_capitalized(string), expected)

    def test_title(self):
        string = "hello world"
        expected = "Hello World"
        self.assertEqual(make_title(string), expected)


if __name__ == "__main__":
    unittest.main()
