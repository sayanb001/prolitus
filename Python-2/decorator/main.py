#! /usr/bin/python3

def decorations(func):
    print(">> Decoration Before")
    def inner():
        func()
        print("function being decorated: ", func.__name__)
        print(">> Deco after function call")
    return inner


def hello_world1():
    print("Hello World")

hello_world1()
print()

@decorations
def hello_world2():
    print("Hello World")

hello_world2()
