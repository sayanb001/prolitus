#! /usr/bin/python3
import subprocess as sb
def show(grid):
    for row in grid :
      print(''.join(row))

def clear():
    sb.run(['clear'])

def play(grid, num, op):
  for i, row in enumerate(grid):
    if num < 10 and num > 0:
      try:
        row[row.index(str(num))] = op
        return True
      except:
        continue
    else:
      print("\n[err] enter value b/w 1 and 9\ntry again.\n")

def win_check(grid):
    # print(grid[0][0], grid[0][2], grid[0][4])
    if grid[0][0] == grid[0][2] == grid[0][4]:
        return True
    if grid[2][0] == grid[2][2] == grid[2][4]:
        return True
    if grid[4][0] == grid[4][2] == grid[4][4]:
        return True
#   -----------------------------------------
    if grid[0][0] == grid[2][0] == grid[4][0]:
        return True
    if grid[0][2] == grid[2][2] == grid[4][2]:
        return True
    if grid[0][4] == grid[2][4] == grid[4][4]:
        return True
#   -----------------------------------------
    if grid[0][0] == grid[2][2] == grid[4][4]:
        return True
    if grid[0][4] == grid[2][2] == grid[4][0]:
        return True

    return False

