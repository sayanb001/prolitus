#! /usr/bin/python3
# import subprocess as sb
from util import *

grid = [['1', ' | ', '2', ' | ', '3'],
        ['--', '--', '--', '--', '--'],
        ['4', ' | ', '5', ' | ', '6'],
        ['--', '--', '--', '--', '--'],
        ['7', ' | ', '8', ' | ', '9']]

count = 1
op = ""
player = ""
won = False
clear()

while not won:
  if win_check(grid):
    w = f"Player {player} - {op} WON"
    print(w)
    print('-'* len(w), '\n')
    show(grid)
    won = True
    continue

  if count % 2:
    player = '1'
    op = 'X'
  else:
    player = '2'
    op = 'O'

  print(f"Player {player} - {op}")
  show(grid)
  try:
    pos = int(input("Enter position [1-9] | (999 - exit): "))
    if(pos == 999):
        exit()
    if play(grid, int(pos), op):
      count += 1
  except:
      clear()
      print("Wrong input")
      continue
  clear()

