$(document).ready(function() {
    $(".num").each(function() {
        console.log($(this).html())
    })

    $(".num:eq(2)").html("New Content")
    $(".num:eq(2)").append(" APPEND") // inside element, at the end
    $(".num:eq(2)").after("-->after element") // after the element

    $(".num:eq(4)").html("New Content") // inside element, before content
    $(".num:eq(4)").prepend("PREPEND ") // before the element
    $(".num:eq(4)").before("-->before element")

    var hello = "hello", space = " ", world = "world"
    $(".num:eq(0)").append(hello,space, world)


})
