$(document).ready(function(){
  $("#submit").on("click", function(){
    var data = $("#mt").val()
    $("#item_list").append("<li class='item'>"  + data + "</li>")
  });

  // Event Delegation
  $("#item_list").on("click", "li",function(){
    if(!$(this).hasClass("done")){
      $(this).addClass("done")
      $(this).append(" &#10003;")
    }
    else{
      $(this).removeClass("done")
      $(this).text($(this).text().split(" ")[0])
    }
  })
});
