var btns = document.getElementsByClassName("btns")
var output = document.getElementById("output")
var errbox = document.getElementById("errbox")
errbox.innerHTML = "";

for(let x of btns){
    x.addEventListener("click", function(){
        output.innerHTML = "Clicked from " + x.innerHTML
        if(x.innerHTML.split(" ")[2] == "1"){
            output.style.border = "3px solid blue"
        }
        else {
            output.style.border = "3px solid red"
        }
    })
}

// === validate form === //

var form = document.getElementById("myForm")
form.onsubmit = function(){
    var name = document.getElementById("name")
    var email = document.getElementById("email")
    var age = document.getElementById("age")
    var err_msg = ""

    if(name.value == ""){
        err_msg += "Name not provided </br>"
    }
    if(age.value == ""){
        err_msg += "Age not provided </br>"
    }
    if(email.value == ""){
        err_msg += "Email not provided </br>"
    }
    if(!validate(email.value)){
        err_msg += "Not a Valid Email"
    }
    if(err_msg.length > 0){
        errbox.style.display = "block"
        errbox.innerHTML = err_msg
        return false
    }
    else { return true; }
}

function validate(em){
     var reg = /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/
    return reg.test(em);
    // return false
}

