var a = [1,2,3,4,5]
console.log("Trying slicing and splicing")
console.log(a.slice(2,4)) // 2,3,4
console.log(a.splice(2,2,30,40)) //[1,2,30,40,5]
console.log(a)
console.log("----")

console.log("split and join")
var name = "sayan";
console.log("before: " + name)
console.log("after: " + name.split("").join("-"));
console.log("----")

console.log("Objects  and Constructors in JS")
console.log("Obj 1")
var myObj = new Object()
myObj.first = "Sayan"
myObj.last = "Bhowmik"
myObj.age = "22"
console.log(myObj)

console.log("Obj 2")
var myObj2 = {type:"Sedan", color:"Red", model:"XYZ"}
console.log(myObj2)

function Shape(length, width){
    this.length = length
    this.width = width
    this.color = "black" }
console.log("shape object")
var shape1 = new Shape(10,5)
console.log(shape1)
console.log("----")




