$(document).ready(function() {
    $("#b-get").on("click",function(e){
        $.get("req.php", function(data){
            console.log(data)
        })
    })

    $("#b-post").on("click",function(e){
        var iput = $("#idata").val()
        var postData = {
            data: iput,
        }
        $.post("req.php", postData, function(data, status, xhr){
            console.log(xhr.postData)
            $("#output").text(data)
        });
    })

    $("#b-ajax").on("click",function(e){
        var iput = $("#idata").val()
        var postData = {
            data: iput,
        }
        $.ajax({
            url: "req.php",
            data: iput,
            success: function(data, status,xhr){
                console.log(status)
                console.log("AJAX call: 1")
            },
            error: function(data, status, xhr){
                console.log(status)
                console.log("AJAX call: 0")
            },
        })
    })
})
