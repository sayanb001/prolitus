var json_url = "https://api.myjson.com/bins/gdtr4"
var xhr = new XMLHttpRequest();
var json_data = ""
var output = document.getElementById("output")
var but1 = document.getElementById("but1")
but1.onclick = getData

function getData(){
    output.innerHTML = "Loading"
    xhr.open("GET", json_url, true);
    xhr.onreadystatechange = function(){
        if(this.readyState == 4){
            output.innerHTML = ""
            var obj = JSON.parse(this.responseText)
            var count = 1;
            for(let person of obj){
                var ne = document.createElement('p')
                var msg = ""
                msg += "<h3><u>" + count++ + "</h3></u>"
                msg += "<b>Name: </b>" + person.name + "</br>"
                msg += "<b>Age: </b>" + person.age + "</br>"
                msg += "<b>Company: </b>" + person.company + "</br>"
                msg += "<b>Phone: </b>" + person.phone + "</br>"
                msg += "<hr>"
                // var nte = document.createTextNode(msg)
                // ne.appendChild(nte);
                ne.innerHTML = msg
                output.appendChild(ne);
            }
        }
    }
    xhr.send();
}
