var submit = document.getElementById("submit")
submit.addEventListener("click", myFun)

function myFun(){
    var name = document.getElementById("name").value
    var age  = document.getElementById("age").value
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if (xhr.readyState == 4 && xhr.status == 200){
            console.log("debug: from readystate")
            console.log(xhr)
            document.getElementById("output").innerHTML = xhr.responseText;
        }
    }

    xhr.open("POST", "req.php", true)
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded")
    // console.log(name + age)
    xhr.send("name=" + name + "&age=" + age)
}
