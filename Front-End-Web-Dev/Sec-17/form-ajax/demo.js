$(document).ready(function() {

    $("#myform").submit(function(e) {
        e.preventDefault() // disable submit function
    })

    $("#submit").on("click", function() {
        console.log("hello")
        var name = $("#name").val()
        var email = $("#email").val()
        var gender = ""
        var pwd = $("#pwd2").keyup(checkPass)
        var sel = $("#sel").prop('value');
        var hobbies = []

        // gender
        if ($("#rdo1").prop("checked")) {
            gender = "male"
        } else if ($("#rdo2").prop("checked")) {
            gender = "female"
        } else if ($("#rdo3").prop("checked")) {
            gender = "other"
        } else { gender = "not provided" }

        // hobies
        $("#myForm").find("#ck1, #ck2, #ck3").each(function() {
            // console.log($(this))
            // [WIP]
        })

        // console.log(hobbies)
        // hobbies = hobbies.join(",")

        // data submit
        if (pwd && $("#pwd1").val().length > 0) {
            console.log(name)
            console.log(email)
            console.log(gender)
            console.log(sel)
            // console.log(hobbies)
            $.ajax({
                url: "req.php",
                type: "POST",
                data: {
                    'name': name,
                    'email': email,
                    'gender': gender,
                    'subject': sel,
                },
                success: function(data, status, xhr) {
                    $("#output").html(data)
                    // console.log(xhr)
                },
                error: function(data, status, xhr) {
                    console.log(status)
                    console.log(xhr)
                }
            })
        } else { console.log("pwd not matched") }
    })

    $("#pwd2").keyup(checkPass)

    function checkPass() {
        var pwd1 = $("#pwd1").val()
        if (pwd1 != $(this).val()) {
            $("#output").text("Passwords do not match")
            $(this).removeClass("pwd-ok")
            $("#pwd1").removeClass("pwd-ok")
            return false
        } else {
            $("#output").text("Passwords match")
            $(this).toggleClass("pwd-ok")
            $("#pwd1").toggleClass("pwd-ok")
            return true
        }
        return false
    }

})
