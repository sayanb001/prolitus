#! /usr/bin/python3
from datetime import datetime as dt
files = ['file1', 'file2', 'file3']
out_file = (str(dt.now()).replace(':','-').replace(' ','-').replace('.','-')) + '.txt'


with open(out_file, 'w') as out:
    for f in files:
        open_file = open(f,'r')
        content = open_file.read()
        out.write(content)
        out.write('\n')
        open_file.close()
