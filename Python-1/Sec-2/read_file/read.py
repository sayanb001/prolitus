contents = open('file.txt').read().splitlines()
i1 = "Banana"
i2 = "Orange"

if i1 in contents:
    print(i1, "in content file")
else:
    print(i1, "not in content file")

if i2 in contents:
    print(i2, "in content file")
else:
    print(i2, "not in content file")
