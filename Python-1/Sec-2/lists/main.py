# using conditionals within loops
a = [1,2,3,4,5]

# approach one
for item in a:
    if(item > 2):
        print(item)

print("------\n")
# alternate way
b = list(str(x) for x in a if x > 2)
print('\n'.join(b))
