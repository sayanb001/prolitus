my_list = ["adam", "jake", "aaron"]
print(my_list)
print("Before adding: ")
print(hex(id(my_list)))

my_list.append("mike")
my_list.append("jackie")

print("\nAfter adding: ")
print(hex(id(my_list)))
print(my_list)
