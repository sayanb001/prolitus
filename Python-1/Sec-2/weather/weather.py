# weather convertor from C to F and vice versa

def c_to_f(temp):
    return ((9 * temp) / 5) + 32

def f_to_c(temp):
    return ((temp - 32) * 5) / 9


t_c = 30
t_f = c_to_f(t_c)
t_c = f_to_c(t_f)

print(t_c, u'\u00b0 C')
print(t_f, u'\u00b0 F')

