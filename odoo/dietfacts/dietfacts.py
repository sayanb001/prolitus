# Diet facts app
from openerp import models, fields, api
from math import floor


class DietFacts_product_template(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    calories = fields.Integer("Calories")
    serving_size = fields.Float("Serving Size")
    last_updated = fields.Date("Last Updated")
    nutrient_ids = fields.One2many('product.template.nutrient',
                                   'product_id', 'Nutrients')

    @api.one
    @api.depends('nutrient_ids', 'nutrient_ids.val')
    def _calcscore(self):
        currentscore = 0
        for nutrient in self.nutrient_ids:
            if nutrient.nutrient_id.name == 'Sodium':
                currentscore += (nutrient.val) + 2
            elif nutrient.nutrient_id.name == 'Fibers':
                currentscore += (nutrient.val) * 3
            else:
                currentscore += nutrient.val
        self.nutrient_score = currentscore

    nutrient_score = fields.Float(string="Nutrition Score",
                                  compute="_calcscore", store=True)


class DietFacts_res_users_meal(models.Model):
    _name = 'res.users.meal'
    _description = 'User Meals'
    name = fields.Char("Meal Name")
    meal_date = fields.Datetime("Meal Date")
    item_ids = fields.One2many('res.users.mealitem', 'meal_id')
    user_id = fields.Many2one('res.users', 'Meal User')
    notes = fields.Text('Meal Notes')

    @api.onchange('totalcal')
    def check_totalcal(self):
        if self.totalcal > 500:
            self.largemeal = True
        else:
            self.largemeal = False

    largemeal = fields.Boolean("Large Meal")

    @api.one
    @api.depends('item_ids')
    def _calc123(self):
        currentcalories = 0
        for mealitems in self.item_ids:
            currentcalories += int(mealitems.calories * mealitems.servings)
        self.totalcal = currentcalories

    totalcal = fields.Integer(string='Total Calories',
                              store=True, compute='_calc123')


class DietFacts_res_users_mealitem(models.Model):
    _name = 'res.users.mealitem'
    meal_id = fields.Many2one('res.users.meal')
    item_id = fields.Many2one('product.template', 'Menu Item')
    calories = fields.Integer(related='item_id.calories',
                              store=True,
                              readonly=True,
                              string="Calories x serving")
    servings = fields.Float('Servings')
    notes = fields.Text('Meal item notes')

# ------------------------------------------------------------------------------


class DietFacts_product_nutrient(models.Model):
    _name = 'product.nutrient'
    name = fields.Char("Nutrient Name")
    uom_id = fields.Many2one('product.uom', 'Unit of Measure')
    desc = fields.Text("Nutrient Description")


class DietFacts_product_template_nutrient(models.Model):
    _name = 'product.template.nutrient'
    nutrient_id = fields.Many2one('product.nutrient', "Product Nutrient")
    product_id = fields.Many2one('product.template')
    val = fields.Float('Nutrient Value')
    uom = fields.Char(related='nutrient_id.uom_id.name',
                      string="UOM", readonly=True)
    dailyval = fields.Float('Daily recomended value')
