import xmlrpclib
import csv

server = 'http://localhost:8069'
database = 'dietfacts2'
user = 'df2@df2.com'
pwd = 'df2'

# common = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/2/common/')
common = xmlrpclib.ServerProxy('%s/xmlrpc/2/common' % server)
# print common.version()

uid = common.authenticate(database, user, pwd, {})
# print uid

OdooAPI = xmlrpclib.ServerProxy('%s/xmlrpc/2/object' % server)

filter = [('largemeal', '=', 'True')]
# product_count = OdooAPI.execute_kw(database, uid, pwd, 'res.users.meal',
                    # 'search_count', [[]])
pt = OdooAPI.execute_kw(database, uid, pwd,
                        'res.users.meal', 'search_count', [filter])

# print pt

filename = 'products.csv'
reader = csv.reader(open(filename, 'rb'))


cat_filter = [('name', '=', 'Diet Item')]
cat_id = OdooAPI.execute_kw(database, uid, pwd, 'product.category',
                            'search', [cat_filter])
for row in reader:
    product = row[0]
    cal = row[1]

    filter = [('name' , '=', product)]
    product_id = OdooAPI.execute_kw(database, uid, pwd, 'product.template',
                                    'search', [filter])
    if product_id:
        print "Exists - " + product
    else:
        record = [{'name' : product, 'calories' : cal, 'categ_id': cat_id[0]}]
        OdooAPI.execute_kw(database, uid, pwd, 'product.template', 'create',
                           record)
        print "+> Added Product - " + product

def update (pname):
        filter = [('name' , '=', pname)]
        product_id = OdooAPI.execute_kw(database, uid, pwd, 'product.template',
                                        'search', [filter])
        if product_id:
            print "Found"
            record = {'name' : 'notChicken', 'calories' : '312', 'categ_id':cat_id[0]}
            OdooAPI.execute_kw(database, uid, pwd, 'product.template',
                               'write', [product_id,record])
            print "Updated"


        else:
            print "Product does not exist"

update('Chicken')
