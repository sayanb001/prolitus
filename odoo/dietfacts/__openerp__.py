{
    'name' : "DietFacts",
    'version' : "1.0",
    'author' : "Greg Moss, OdooClass.com",
    'description':'Add nutrition facts to product',
    'depends' : ['sale'],
    'data': ['dietfacts_view.xml', 'security/ir.model.access.csv'],
    'installable' : True,
}
